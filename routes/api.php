<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'links'], function() {
    Route::get('extract', 'LinksController@getLinksForVideoLinkExtraction');
    Route::post('extract/save', 'LinksController@saveLinksWithOriginalVideoSource');
    Route::get('bad', 'LinksController@getBadLinks');
    Route::post('bad/update', 'LinksController@updateBadLinks');
    Route::post('bad/link/update', 'LinksController@updateBadLinkById');
    Route::post('/new/link', 'LinksController@addNewLinks');
    Route::get('/episode/empty/thumbs', 'LinksController@getSeriesEpisodesWithNoThumb');
    Route::post('/episode/empty/thumbs', 'LinksController@saveSeriesEpisodesThumbs');
});
Route::group(['prefix'=>'services'], function() {
    Route::group(['prefix'=>'anime'], function() {
        Route::post('/new/episodes', 'AnimeServiceEpisodeController@saveNewEpisode');
    });
});


Route::group(['prefix'=>'index'], function() {
    Route::get('/', 'HomeController@index');
});

Route::group(['prefix'=>'series'], function() {
    Route::get('/{slug}', 'SeriesController@getSeriesBySlug');
    Route::get('/new/added', 'SeriesController@getNewAddedSeries');
    Route::get('/{animeSlug}/exists', 'SeriesController@animeExists')->name('animeExists');
});

Route::group(['prefix'=>'episode'], function() {
   Route::get('/{slug}/series/{seriesSlug}', 'EpisodeController@getEpisodeDetails');
   Route::get('/{episodeId}/embedded/url', 'EpisodeController@fetchEmbeddedUrlForEpisode');
   Route::get('/add/new', 'EpisodeController@getNewEpisodes');
    Route::get('/{episodeSlug}/series/{animeSlug}/exists', 'EpisodeController@episodeExists')->name('episodeExists');
});

Route::group(['prefix'=>'genre'], function() {
   Route::get('/{slug}/series', 'GenreController@getGenreSeriesBySlug');
});

Route::group(['prefix'=>'search'], function() {
    Route::get('/{search}', 'SearchController@search');
});

Route::group(['prefix'=>'browse'], function() {
    Route::get('/', 'SeriesController@getBrowseSeries');
});

Route::group(['prefix'=>'crawler'], function() {
    Route::post('/link', 'Crawler\SeriesDetailsController@saveCrawlerLink');
    Route::post('/series/details', 'Crawler\SeriesDetailsController@createEpisodeDetails');
    Route::get('/links', 'Crawler\SeriesDetailsController@getSeriesLinks');
});

Route::group(['prefix'=>'contact'], function() {
    Route::post('/', 'ContactController@index');
});

Route::group(['prefix'=>'votes'], function() {
    Route::post('/like', 'VoteController@like');
    Route::post('/dislike','VoteController@dislike');
});

Route::group(['prefix'=>'pages'], function() {
    Route::post('/episode/{episodeSlug}/series/{seriesSlug}/view', 'ViewController@episodePageView');
});
