<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    static $IMAGE_OPTIMIZED = 1;
    protected $guarded = ['id'];

    protected $appends = array('image_path', 'poster_image_path');

    /**
     * @return string
     */
    public function getImagePathAttribute()
    {
        if ( ! is_null($this->getAttribute('series_id'))) {
            return getenv("IMAGE_BASE_PATH") .'images/series/' . $this->getAttribute('path');
        } else {
            return getenv("IMAGE_BASE_PATH") .'images/episodes/' . $this->getAttribute('path');
        }

    }

    /**
     * @return string
     */
    public function getPosterImagePathAttribute()
    {
        if ( ! is_null($this->getAttribute('series_id'))) {
            if (is_null($this->getAttribute('resized_image_path'))) {
                return env("IMAGE_BASE_PATH") . 'images/series/' . $this->getAttribute('path');
            }
            return env("IMAGE_BASE_PATH") . 'images/series/' . $this->getAttribute('resized_image_path');
        } else {
            return env("IMAGE_BASE_PATH") .'images/episodes/' . $this->getAttribute('path');
        }

    }

    /**
     * @param $seriesId
     *
     * @return array
     */
    public function getSeriesImages($seriesId) {
        $images = $this->where('series_id', $seriesId);

        return [
          'cover'=>$images->where('type', 'cover')->all(),
          'poster'=>$images->where('type', 'poster')->all()
        ];
    }
}
