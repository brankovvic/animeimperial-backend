<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EpisodeRating extends Model
{
    static $LIKE = 1;

    static $DISLIKE = 2;

    static $REDIS_CACHE_TIME = 3600;

    protected $guarded = ['id'];
}
