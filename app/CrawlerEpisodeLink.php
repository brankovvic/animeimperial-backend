<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrawlerEpisodeLink extends Model
{
    protected $guarded = ['id'];
}
