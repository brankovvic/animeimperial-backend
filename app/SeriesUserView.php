<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeriesUserView extends Model
{
    protected $guarded = ['id'];
}
