<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    const SERIES_PATH_NAME = 'series';

    const INITIAL = 0;

    const SEASON_IMPORT = 1;

    const EPISODE_IMPORT = 2;

    const COMPLETED = 4;

    const WATCH_LINKS_IMPORT = 3;

    const BAD = 2;

    const MOVIE = 1;

    protected $guarded = ['id'];

    protected $appends = ['totalEpisodeCount'];

    public function getSeriesStatus(int $status)
    {
        switch($status) {
            case 1:
                return "Import Seasons";
            case 2:
                return "Import Episodes";
            case 3:
                return "Import Watch Links";
            case 4:
                return "Completed";
            default:
                return "Default";
        }
    }

    public function episodes()
    {
        return $this->hasMany(Episode::class, 'series_id', 'id');
    }

    public function getEpisodeBySlug(string $slug)
    {
        return $this->episodes()->where('slug', $slug);
    }

    public function statistics()
    {
        return $this->hasOne(SeriesStatistic::class);
    }

    public function genres()
    {
        return $this->hasMany(SeriesGenre::class, 'series_id', 'id')->join('genres', 'genres.id', '=', 'series_genres.genre_id');
    }

    public function getTotalEpisodeCountAttribute()
    {
        return $this->episodes()->count();
    }
    public function images()
    {
        return $this->hasMany(Image::class, 'series_id', 'id');
    }
}
