<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrawlerLink extends Model
{
    const INITIAL = 0;

    const COMPLETED = 1;

    const BAD = 2;

    protected $guarded = ['id'];
}
