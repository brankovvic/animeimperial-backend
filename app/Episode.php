<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
    static $EPISODE_PATH_NAME = "episodes";

    protected $guarded = ['id'];

    protected $appends = ['created', 'updated', 'pageViewCount'];

    public function images()
    {
        return $this->hasMany(Image::class, 'episode_id', 'id');
    }

    public function series()
    {
        return $this->belongsTo(Series::class);
    }

    public function link()
    {
        return $this->hasOne(Links::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function links()
    {
        return $this->hasMany(Links::class);
    }

    public function getCreatedAttribute()
    {
        if( ! is_null($this->created_at)) {
            return $this->created_at->diffForHumans();
        }
    }

    public function getUpdatedAttribute()
    {
        if( ! is_null($this->created_at)) {
            return $this->created_at->diffForHumans();
        }
    }

    public function pageView()
    {
        return $this->hasMany(PageView::class);
    }

    public function getPageViewCountAttribute()
    {
        return $this->pageView()->count();
    }
}
