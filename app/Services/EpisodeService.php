<?php

namespace App\Services;

use App\Episode;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class EpisodeService
{
    /**
     * @var Episode
     */
    private $episode;

    public function __construct(Episode $episode)
    {

        $this->episode = $episode;
    }

    /**
     * @param int $startingId
     *
     * @return Collection
     */
    public function getWatchEpisodeStream(Episode $episode)
    {
        return $this->episode->with('images', 'link')
            ->where('series_id', $episode->series_id)
            ->where('episode_number', '>', $episode->episode_number)
            ->orderBy('episode_number', 'ASC')->take(15)->get();
    }

    /**
     * @return array
     */
    public function getNewAddedEpisodes():array {

       $episodes = $this->episode->with('links', 'images', 'series', 'series.genres')
           ->where('created_at', '>=', Carbon::now()->subDays(30)->toDateTimeString())
           ->orderBy('series_id')
           ->orderBy('episode_number')
           ->get()
           ->toArray();

       $episodesCount = count($episodes);

       $newEpisodes = [];

       for ($i=0;$i<$episodesCount;$i++) {

           if (!empty($episodes[$i + 1])) {
               if ($episodes[$i]['series_id'] === $episodes[$i+1]['series_id']) {
                   continue;
               } else {
                   array_push($newEpisodes, $episodes[$i]);
               }
           } else {
               array_push($newEpisodes, $episodes[$i]);
           }
       }

       return $newEpisodes;
    }
}
