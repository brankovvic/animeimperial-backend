<?php

namespace App\Services;

class CrawlerImageDownload
{
    public $baseImagePath = "https://image.tmdb.org/t/p/original";

    public function createSeriesImageDir($seriesId)
    {
        $imagePath = implode('/', str_split($seriesId));
        if (file_exists(public_path('images/series/' . $imagePath))) return $imagePath . '/';

        mkdir(public_path('images/series/' . $imagePath), 0777, true);

        return $imagePath . '/';
    }

    public function createSeasonImageDir($seasonId)
    {
        $imagePath = implode('/', str_split($seasonId));
        if (file_exists(public_path('images/season/' . $imagePath))) return $imagePath . '/';

        mkdir(public_path('images/season/' . $imagePath), 0777, true);

        return $imagePath . '/';
    }

    /**
     * @param $episodeId
     *
     * @return string
     */
    public function createEpisodeImageDir($episodeId): string
    {
        $imagePath = implode('/', str_split($episodeId));

        if (file_exists(public_path('images/episodes/' . $imagePath))) return $imagePath;

        mkdir(public_path('images/episodes/' . $imagePath), 0777, true);

        return $imagePath;
    }

    /**
     * @param $imageName
     * @param $imagePath
     * @param $remoteImageUrl
     * @param $type
     *
     * @return string
     */
    public function saveImageToDir($imageName, $imagePath, $remoteImageUrl, $type): string
    {
        if(file_exists(public_path("images/{$type}/" . $imagePath . $imageName))) return $imagePath . $imageName;

        $in=    fopen($remoteImageUrl, "rb");
        $out=   fopen(public_path("images/{$type}/" . $imagePath . $imageName), "wb");
        while ($chunk = fread($in,8192))
        {
            fwrite($out, $chunk, 8192);
        }
        fclose($in);
        fclose($out);

        return $imagePath . $imageName;
    }
}
