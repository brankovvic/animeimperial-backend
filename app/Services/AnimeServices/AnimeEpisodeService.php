<?php

namespace App\Services\AnimeServices;

use App\Jobs\AddNewSeries;
use App\Series;

class AnimeEpisodeService {
    /**
     * @var Series
     */
    private $series;
    /**
     * @var AddNewSeries
     */
    private $addNewSeriesJob;

    public function __construct(Series $series)
    {

        $this->series = $series;
    }

    public function saveNewRecentEpisode(array $episode) {
        $series = $this->series->where('title', $episode['seriesTitle'])
            ->orWhere('slug', str_slug($episode['seriesTitle']))
            ->first();

        if (is_null($series)) {
            dispatch(new AddNewSeries($episode));
        }
    }
}
