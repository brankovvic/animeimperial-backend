<?php

namespace App\Services;

use App\EpisodeRating;
use App\EpisodeView;
use Carbon\Carbon;

class EpisodeVotesService
{
    /**
     * @var EpisodeView
     */
    private $episodeView;

    public function __construct(EpisodeRating $episodeView)
    {

        $this->episodeView = $episodeView;
    }

    public function like(int $episodeId, string $clientIp)
    {
        $episodeView = $this->episodeView->where('client_ip', $clientIp)->where(
            'episode_id',
            $episodeId
        )->first()
        ;
        if (!is_null($episodeView)) {
            if (!Carbon::now()->gt($episodeView->last_voted)) {
                $this->saveToDatabase(EpisodeRating::$LIKE, $episodeId, $clientIp);
            }
        } else {
            $this->saveToDatabase(EpisodeRating::$LIKE, $episodeId, $clientIp);
        }
    }

    public function dislike(int $episodeId, string $clientIp)
    {
        $episodeView = $this->episodeView->where('client_ip', $clientIp)->where(
            'episode_id',
            $episodeId
        )->first()
        ;
        if (!is_null($episodeView)) {
            if (!Carbon::now()->gt($episodeView->last_voted)) {
                $this->saveToDatabase(EpisodeRating::$DISLIKE, $episodeId, $clientIp);
            }
        } else {
            $this->saveToDatabase(EpisodeRating::$DISLIKE, $episodeId, $clientIp);
        }
    }

    private function saveToDatabase(int $type, int $episodeId, string $clientIp)
    {
        if ($type === EpisodeRating::$LIKE) {
            $this->episodeView->create(
                [
                    'episode_id' => $episodeId,
                    'like' => 1,
                    'client_ip' => $clientIp,
                    'last_voted' => Carbon::now(),
                ]
            );
        } else {
            $this->episodeView->create(
                [
                    'episode_id' => $episodeId,
                    'dislike' => 1,
                    'client_ip' => $clientIp,
                    'last_voted' => Carbon::now(),
                ]
            );
        }
    }

    /**
     * @param int $episodeId
     */
    public function votesCount(int $episodeId): int {
        return $this->episodeView->where('episode_id', $episodeId)->count();
    }

    /**
     * @param int $episodeId
     */
    public function likes(int $episodeId): int  {
        return $this->episodeView->where('episode_id', $episodeId)->where('like', '!=', null)->count();
    }

    /**
     * @param int $episodeId
     */
    public function dislikes(int $episodeId): int  {
        return $this->episodeView->where('episode_id', $episodeId)->where('dislike', '!=', null)->count();
    }

    public function userVoted(int $episodeId, $ipAddress) {
        $like = $this->episodeView->where('episode_id', $episodeId)->where('client_ip', $ipAddress)->first();

        if($like === null) return false;

        return ($like->like === null) ? "dislike" : "like";

    }
}
