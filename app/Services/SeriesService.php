<?php

namespace App\Services;

use App\Series;
use App\SeriesView;
use Illuminate\Database\Eloquent\Collection;

class SeriesService
{
    /**
     * @var SeriesView
     */
    private $seriesView;

    public function __construct(Series $series, SeriesView $seriesView)
    {

        $this->seriesView = $seriesView;
    }

    /**
     * @return Collection
     */
    public function getMostViewed(): Collection
    {
        return $this->seriesView->with('genres')
            ->whereHas('episodes', function($query) {
                $query->where('title', '!=',null);
                $query->where('slug', '!=', null);
            })
            ->orderBy('viewNumber', 'DESC')->limit(12)->get();
    }

    public function getTopRated(): Collection
    {
        return $this->seriesView->with('genres')
            ->whereHas('episodes', function($query) {
                $query->where('title', '!=',null);
                $query->where('slug', '!=', null);
            })
            ->where('ratingCount', '>=', 100)
            ->orderBy('ratingNumber', 'DESC')
            ->limit(12)
            ->get();
    }

    public function getTopScore(): Collection
    {
        return $this->seriesView->with('genres')
            ->whereHas('episodes', function($query) {
                $query->where('title', '!=',null);
                $query->where('slug', '!=', null);
            })
            ->orderBy('scoreCount', "DESC")
            ->orderBy('scoreNumber', 'DESC')->limit(12)->get();
    }

    public function getRecentAddedAnime()
    {
        return $this->seriesView->with('genres')
            ->whereHas('episodes', function($query) {
                $query->where('title', '!=',null);
                $query->where('slug', '!=', null);
            })
            ->orderBy('created_at', "DESC")
            ->limit(12)
            ->get();
    }
}
