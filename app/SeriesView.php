<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Feed\Feedable;
use Spatie\Feed\FeedItem;

class SeriesView extends Model implements Feedable
{
    protected $table = "series_view";
    protected $appends = ['images'];
    public function genres()
    {
        return $this->hasMany(SeriesGenre::class, 'series_id', 'id')->join('genres', 'genres.id', '=', 'series_genres.genre_id');
    }

    public function images()
    {
        return $this->hasMany(Image::class, 'series_id', 'id');
    }

    public function episodes()
    {
        return $this->hasMany(Episode::class, 'series_id', 'id')->orderBy('episode_number', 'ASC');
    }

    public function scopeSortBy($query, $sort) {
        if (is_null($sort)) return $query;

        if ($sort === "added") {
            $query->orderBy('created_at', 'DESC');
        } else if($sort === "viewed") {
            $query->orderBy('viewNumber', 'DESC');
        } else if($sort === "rated") {
            $query->orderBy('ratingNumber', 'DESC');
        } else if($sort === "title") {
            $query->orderBy('title', 'DESC');
        } else if($sort === "score") {
            $query->orderBy('scoreNumber', 'DESC');
        } else {
            return $query;
        }

        return $query;
    }

    public function sortBy($sortBy) {
        if (is_null($sortBy)) return ;
    }

    /**
     * @return array|\Spatie\Feed\FeedItem
     */
    public function toFeedItem()
    {

        return FeedItem::create([
            'id'=>$this->id,
            'title'=>$this->title,
            'created'=>$this->created_at,
            'updated'=>$this->updated_at,
            'summary'=>$this->description,
            'image'=> $this->images()->where('type','poster')->first()->poster_image_path,
            'link'=>env('BASE_URL') . 'series/' . $this->slug,
            'author'=>'AnimeImperial.com'
        ]);
    }

    public static function getFeedItems()
    {
        return SeriesView::whereHas('images', function($query) {
            $query->where('type', 'poster');
        })->where('series_completed', Series::COMPLETED)
            ->where('quality', 'HD')
            ->where('movie', '!=', Series::MOVIE)
            ->orderBy('viewNumber', 'DESC')
            ->limit(20)
            ->get();
    }

    public function getImagesAttribute() {
        $images = $this->images()->where('series_id', $this->id)->get();

        return [
            'cover'=>array_values($images->where('type', 'cover')->all()),
            'poster'=>array_values($images->where('type', 'poster')->all())
        ];
    }
}
