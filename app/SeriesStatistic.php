<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeriesStatistic extends Model
{
    protected $guarded = ['id'];
}
