<?php

namespace App\Console\Commands;

use App\Series;
use Illuminate\Console\Command;

class CacheAnime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:anime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $animes = Series::all();

        foreach ($animes as $anime) {
            if (file_exists('/pagesCache/' . md5('/'.$anime->slug).'.html')) {
                unlink('/pagesCache/' . md5('/'.$anime->slug).'.html');
            }

            file_get_contents('https://www.animeimperial.com/' . $anime->slug);
            sleep(1);
        }
    }
}
