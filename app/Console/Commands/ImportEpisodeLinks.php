<?php

namespace App\Console\Commands;

use App\Links;
use App\LinkServer;
use App\Series;
use Illuminate\Console\Command;

class ImportEpisodeLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:episodeLinks {seriesId} {jsonPath} {server?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import episode links by json file and series id
                                format of json is {episodeId:seriesLink} serverName';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $seriesId = $this->argument('seriesId');

        $series = Series::where('id', $seriesId)->with('episodes')->first();

        if(is_null($series)) {
            echo "Bad Series ID\n";
            exit(1);
        }

        $jsonPath = $this->argument('jsonPath');

        if(! file_exists($jsonPath)) {
            echo "Bad json file\n";
            exit(1);
        }

        $links = json_decode(file_get_contents($jsonPath), true);

        $server = $this->argument('server');

        if ( ! is_null($server)) {
            $serverLink = LinkServer::firstOrCreate([
                'name'=> $server
            ]);
        }

        foreach ($links as $episodeId=>$link) {

            $episode = $series->episodes()->where('episode_number', $episodeId)->first();

            if(is_null($episode)) continue;

            if ( ! is_array($link)) {
                $this->createLink($episode->id, $link, $serverLink->id);
            } else {
                foreach ($link['episodeLinks'] as $episodeLink) {

                    $serverLink = LinkServer::firstOrCreate([
                        'name'=> $episodeLink['server']
                    ]);

                    $this->createLink($episode->id, $episodeLink['link'], $serverLink->id);
                }
            }
        }
    }

    private function createLink($episodeId, $link, $serverId) {
        return Links::create([
            'episode_id'=>$episodeId,
            'status'=>Links::$BAD_LINK,
            'link'=>$link,
            'server_id'=>$serverId
        ]);
    }
}
