<?php

namespace App\Console\Commands;

use App\Series;
use Illuminate\Console\Command;

class FixBrokenSeriesDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fixSeries:details';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix Series Details';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $baseUrl = "https://kitsu.io/api/edge/anime?filter[text]=";

        $series = Series::where('aired', 'Completed')->get();

        foreach ($series as $s) {
            $searchUrl = $baseUrl . $s->title;
            echo "title: {$s->title}\n";
            try {
                $seriesResponse = $this->makeRequest($searchUrl);
            } catch (\Exception $exception) {
                continue;
            }

            $s->aired = $seriesResponse['data'][0]['attributes']['startDate'] . " To " . $seriesResponse['data'][0]['attributes']['endDate'];
            $s->status = $seriesResponse['data'][0]['attributes']['status'];
            $s->premiere = $seriesResponse['data'][0]['attributes']['startDate'];
            $s->save();
        }
    }

    private function makeRequest($url): array
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url, [
            'headers'=>[
                'Accept'=>'application/vnd.api+json',
                'Content-Type'=> 'application/vnd.api+json'
            ]
        ]);

        return json_decode($res->getBody(), true);
    }
}
