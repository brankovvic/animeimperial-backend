<?php

namespace App\Console\Commands;

use App\Genre;
use Illuminate\Console\Command;

class CacheGenrePages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:genre';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $genres = Genre::all();

        foreach ($genres as $genre) {
            if (file_exists('/pagesCache/' . md5('/genre/'.$genre->slug).'.html')) {
                unlink('/pagesCache/' . md5('/genre/'.$genre->slug).'.html');
            }
            file_get_contents('https://www.animeimperial.com/genre/' . $genre->slug);
            sleep(1);
        }
    }
}
