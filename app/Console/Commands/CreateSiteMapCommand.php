<?php

namespace App\Console\Commands;

use App\Episode;
use App\Genre;
use App\Series;
use App\SeriesView;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\SitemapIndex;
use Spatie\Sitemap\Tags\Url;


class CreateSiteMapCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates Sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

       $siteMapIndex = SitemapIndex::create();
        $i = 1;
        while(true)
        {

            $items = $this->getDataToSaveInSiteMap($i);

            if ($items->count() === 0) break;

            $siteMap = Sitemap::create();

            foreach ( $items as $item) {
                echo $item->slug . "\n";
                $siteMap->add(Url::create("https://www.animeimperial.com/$item->slug")
                    ->setLastModificationDate(Carbon::now())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
                    ->setPriority(0.6));
            }
            $siteMap->writeToFile(public_path("sitemap/sitemap_page_$i.xml"));
            $siteMapIndex->add("https://www.animeimperial.com/sitemap/sitemap_page_$i.xml");
            $i++;
        }

        $y = 1;
        while(true) {

            $items = $this->getEpisodeDataToSaveInSiteMap($y);

            if ($items->count() === 0) break;

            $siteMap = Sitemap::create();

            foreach ( $items as $item) {
                echo $item->slug . "\n";
                if (is_null($item->slug) || $item->slug === "") continue;
                $siteMap->add(Url::create("https://www.animeimperial.com/".$item->series->slug."/episode/$item->slug")
                    ->setLastModificationDate(Carbon::now())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
                    ->setPriority(0.6));
            }
            $siteMap->writeToFile(public_path("sitemap/sitemap_page_$i.xml"));
            $siteMapIndex->add("https://www.animeimperial.com/sitemap/sitemap_page_$i.xml");
            $y++;
            $i++;
        }

        $siteMapGenre = Sitemap::create();

        $genres = $this->getGenres();

        foreach ($genres as $genre) {
            $siteMapGenre->add(
                Url::create("https://www.animeimperial.com/genre/" . $genre->slug)
                    ->setLastModificationDate(Carbon::now())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
                    ->setPriority(0.6)
            );

        }

        $siteMapGenre->writeToFile(public_path("sitemap/sitemap_genres_.xml"));

        $siteMapIndex->add("https://www.animeimperial.com/sitemap/sitemap_genres.xml");

        $siteMapIndex->writeToFile(public_path("sitemap/sitemap.xml"));
    }

    public function getDataToSaveInSiteMap($currentPage): Collection
    {
        $seriesView = SeriesView::with('genres')
            ->whereHas('episodes', function($query) {
                $query->where('title', '!=',null);
                $query->where('slug', '!=', null);
            })
            ->where('series_completed', Series::COMPLETED)
            ->where('quality', 'HD')
            ->orderBy('ratingCount', 'DESC')
            ->orderBy('ratingNumber', 'DESC');

        return $seriesView->skip(($currentPage - 1) * 30000)->take(30000)->get();
    }

    public function getGenres()
    {
        return Genre::all();
    }

    public function getEpisodeDataToSaveInSiteMap($currentPage): Collection
    {
        $episode = Episode::whereHas('series', function ($query) {
            $query->where('series_completed', Series::COMPLETED);
        })->where('title', '!=',null)->with('series')->where('slug', '!=', null);

        return $episode->skip(($currentPage - 1) * 30000)->take(30000)->get();
    }
}
