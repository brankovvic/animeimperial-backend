<?php

namespace App\Console\Commands;

use App\CrawlerEpisodeLink;
use App\Episode;
use App\Genre;
use App\Image;
use App\Links;
use App\LinkServer;
use App\Series;
use App\SeriesGenre;
use App\SeriesStatistic;
use App\SeriesUserView;
use App\Studio;
use Illuminate\Console\Command;

class ImportSeriesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'series:import {jsonPath}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Series From json file';
    private $mainData = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $jsonPath = $this->argument('jsonPath');

        if (!file_exists($jsonPath)) {
            echo "Bad json file\n";
            exit(1);
        }

        $data = json_decode(file_get_contents($jsonPath), true);

        $genreIds = [];
        foreach ($data['genres'] as $genre) {
            $genre = Genre::firstOrCreate(
                [
                    'name' => $genre,
                    'slug' => str_slug($genre, '-'),
                ]
            );
            array_push($genreIds, $genre->id);
        }

        $studio = Studio::firstOrcreate(
            [
                'name' => $data['studio'],
                'slug' => str_slug($data['studio'], '-'),
            ]
        );

        $series = Series::firstOrCreate(
            [
                'title' => $data['title'],
                'slug' => str_slug($data['title'], '-'),
                'studio_id' => $studio->id,
                'aired' => $data['aired'],
                'status' => $data['status'],
                'premiere' => $data['premiere'],
                'quality' => $data['quality'],
                'description' => $data['description'],
            ]
        );

        foreach ($data['episodes'] as $episodeIndex => $episodeData) {
            $episode = Episode::firstOrCreate(
                [
                    'episode_number' => $episodeIndex,
                    'series_id' => $series->id,
                ]
            );
        }

        foreach ($genreIds as $genreDatabaseId) {
            SeriesGenre::firstOrCreate(
                [
                    'genre_id' => $genreDatabaseId,
                    'series_id' => $series->id,
                ]
            );
        }

        SeriesUserView::firstOrCreate(
            [
                'series_id' => $series->id,
                'view_number' => intval(str_replace(",", "", $data['views'])),
            ]
        );

        SeriesStatistic::firstOrCreate(
            [
                'series_id' => $series->id,
                'rating_number' => floatval($data['ratingNumber']),
                'rating_count' => intval($data['ratingCount']),
                'score_number' => floatval($data['scoreNumber']),
                'score_count' => intval($data['scoreCount']),
            ]
        );
    }

    private function getImageFromUrl($string)
    {
        $start = strrpos($string, '/');

        return substr($string, $start + 1, strlen($string));
    }

    private function createImageDir($seriesId)
    {
        $imagePath = implode('/', str_split($seriesId));
        if (file_exists(public_path('images/' . $imagePath))) {
            return $imagePath;
        }

        mkdir(public_path('images/' . $imagePath), 0777, true);

        return $imagePath;
    }
}
