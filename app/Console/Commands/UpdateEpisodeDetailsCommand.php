<?php

namespace App\Console\Commands;

use App\Episode;
use App\Image;
use App\Series;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class UpdateEpisodeDetailsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'episode:details';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch Episode Details';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $baseUrl = "https://kitsu.io/api/edge/anime?filter[text]=";

        $series = Series::where('series_completed', Series::INITIAL)
            ->where('series_completed','!=', Series::BAD)
            ->where('series_completed','!=', Series::COMPLETED)
            ->limit(1500)->get();

        foreach ($series as $s) {
            $searchUrl = $baseUrl . $s->title;
            echo "title: {$s->title}\n";
            try {
                $seriesResponse = $this->makeRequest($searchUrl);
            } catch (\Exception $exception) {
                continue;
            }

            if (count($seriesResponse['data']) === 0) {
                $s->update(['series_completed'=>Series::BAD]);
                continue;
            }

            $this->saveSeriesImage($seriesResponse['data'], $s);

            $episodesUrl = $seriesResponse['data'][0]['relationships']['episodes']['links']['related'];

            while(true) {
                $episodesData = $this->makeRequest($episodesUrl);

                foreach ($episodesData['data'] as $episodeDataSingle) {
                    if (is_null($episodeDataSingle['attributes']['canonicalTitle'])) {
                    // break bad series
                        $s->update(['series_completed'=>Series::BAD]);
                        break;
                    }
                    try {
                        $episode = $s->episodes()->where('episode_number', $episodeDataSingle['attributes']['number'])->first();

                        $episode->title = $episodeDataSingle['attributes']['canonicalTitle'];
                        $episode->description = $episodeDataSingle['attributes']['synopsis'];
                        $episode->aired = $episodeDataSingle['attributes']['airdate'];
                        $episode->slug = str_slug($episodeDataSingle['attributes']['canonicalTitle']);

                        $episode->save();
                    } catch (\Exception $exception) {
                        $s->update(['series_completed'=>Series::BAD]);
                        break;
                    }

                    try{
                        if (Image::where('episode_id', $episode->id)->where('type', 'poster')->first() === null
                        && ! is_null($episodeDataSingle['attributes']['thumbnail']['original'])) {
                            $imageName = md5(time(). rand()) . '.jpg';
                            $imageDir = $this->createImageDir($episode->id) . '/';

                            $this->saveImageToDir(
                                $imageName,
                                $imageDir,
                                $episodeDataSingle['attributes']['thumbnail']['original'], 'episodes'
                            );

                            Image::firstOrCreate([
                                'episode_id'=>$episode->id,
                                'type'=>'poster',
                                'path'=>$imageDir . $imageName
                            ]);
                        }
                    } catch (\Exception $exception) {
                        echo $exception->getMessage();
                    }
                }

                if (!empty($episodesData['links']['next'])) {
                    $episodesUrl = $episodesData['links']['next'];
                } else {
                    break;
                }
            }

            Artisan::call('resize:images');

            $s->update(['series_completed'=> Series::COMPLETED]);
        }
    }

    private function createImageDir($episodeId)
    {
        $imagePath = implode('/', str_split($episodeId));
        if (file_exists(public_path('images/episodes/' . $imagePath))) return $imagePath;

        mkdir(public_path('images/episodes/' . $imagePath), 0777, true);

        return $imagePath;
    }

    private function createSeriesImageDir($episodeId)
    {
        $imagePath = implode('/', str_split($episodeId));
        if (file_exists(public_path('images/series/' . $imagePath))) return $imagePath . '/';

        mkdir(public_path('images/series/' . $imagePath), 0777, true);

        return $imagePath . '/';
    }

    private function saveImageToDir($imageName, $imagePath, $remoteImageUrl, $type)
    {
        if(file_exists(public_path("images/{$type}/" . $imagePath . $imageName))) return $imagePath . $imageName;

        $in=    fopen($remoteImageUrl, "rb");
        $out=   fopen(public_path("images/{$type}/" . $imagePath . $imageName), "wb");
        while ($chunk = fread($in,8192))
        {
            fwrite($out, $chunk, 8192);
        }
        fclose($in);
        fclose($out);

        return $imagePath . $imageName;
    }

    private function makeRequest($url): array
    {
        $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $url, [
                'headers'=>[
                    'Accept'=>'application/vnd.api+json',
                    'Content-Type'=> 'application/vnd.api+json'
                ]
            ]);

            return json_decode($res->getBody(), true);
    }

    private function saveSeriesImage($data, $s) {
        $imageDir = $this->createSeriesImageDir($s->id);

        if (is_null(Image::where('series_id', $s->id)->where('type', 'poster')->first())) {
            $imageName = md5(time() . rand()) . '.jpg';

            if ($data[0]['attributes']['posterImage'] !== null ) {
                try{
                    if (! is_null($data[0]['attributes']['posterImage']['original'])) {
                        $this->saveImageToDir(
                            $imageName,
                            $imageDir,
                            $data[0]['attributes']['posterImage']['original'],
                            'series'
                        );

                        Image::firstOrCreate(
                            [
                                'series_id' => $s->id,
                                'path' => $imageDir . $imageName,
                                'type' => 'poster'
                            ]
                        );
                    }
                } catch (\Exception $exception) {

                }
            }
        }
        if (is_null(Image::where('series_id', $s->id)->where('type', 'cover')->first())) {
            $coverImageName = md5(time() . rand()) . '.jpg';
            if ($data[0]['attributes']['coverImage'] !== null ) {
                try{
                    $this->saveImageToDir(
                        $coverImageName,
                        $imageDir,
                        $data[0]['attributes']['coverImage']['original'], 'series'
                    );

                    Image::firstOrCreate(
                        [
                            'series_id' => $s->id,
                            'path' => $imageDir . $coverImageName,
                            'type' => 'cover'
                        ]
                    );
                } catch (\Exception $exception) {

                }
            }
        }
    }
}
