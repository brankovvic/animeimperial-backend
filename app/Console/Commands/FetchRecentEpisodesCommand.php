<?php

namespace App\Console\Commands;

use function App\Helpers\makeGetRequest;
use App\Services\AnimeServices\AnimeEpisodeService;
use Illuminate\Console\Command;

class FetchRecentEpisodesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retch:recentEpisodes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches Recent Episodes';
    /**
     * @var AnimeEpisodeService
     */
    private $animeEpisodeService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AnimeEpisodeService $animeEpisodeService)
    {
        parent::__construct();
        $this->animeEpisodeService = $animeEpisodeService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = makeGetRequest(env('SERVICES_ADDRESS') . 'anime/episode/new', []);

        if (is_null($response) || $response === false) return null;

        foreach ($response as $episode) {
            $this->animeEpisodeService->saveNewRecentEpisode($episode);
        }
    }
}
