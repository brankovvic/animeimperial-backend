<?php

namespace App\Console\Commands;

use App\Episode;
use App\Image;
use App\Series;
use App\Services\CrawlerImageDownload;
use Illuminate\Console\Command;

class ImportEpisodeDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:episodeDetails {seriesId} {jsonPath}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import episode details by json file and series id format of json';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $crawlerImageDownload = new CrawlerImageDownload();

        $seriesId = $this->argument('seriesId');

        $series = Series::where('id', $seriesId)->with('episodes')->first();

        if(is_null($series)) {
            echo "Bad Series ID\n";
            exit(1);
        }

        $jsonPath = $this->argument('jsonPath');

        if(! file_exists($jsonPath)) {
            echo "Bad json file\n";
            exit(1);
        }

        $details = json_decode(file_get_contents($jsonPath), true);

        foreach ($details as $episodeNumber=>$detail) {
            $episode = $series->episodes()->where('episode_number', $episodeNumber)->first();
            if (is_null($episode)) dd($episodeNumber);

            if ( !is_null($episode->title)) continue;

            $episode->title = $detail['title'];
            $episode->slug = str_slug($detail['title'], '-');
            $episode->save();

            $episodePath = $crawlerImageDownload->createEpisodeImageDir($episode->id);

            $imageName = md5(time() . rand()) . '.jpg';

            $fullImagePath = $crawlerImageDownload->saveImageToDir(
                $imageName,
                $episodePath,
                $detail['image'],
                Episode::$EPISODE_PATH_NAME
            );

            Image::create(
                [
                    'path' => $fullImagePath,
                    'type' => 'poster',
                    'episode_id' => $episode->id,
                    'image_optimized' => 0
                ]
            );
        }
    }
}
