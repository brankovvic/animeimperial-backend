<?php

namespace App\Console\Commands;

use App\Image;
use Illuminate\Console\Command;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class OptimizeImagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'optimize:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Optimize images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $optimizerChain = OptimizerChainFactory::create();

        $images = Image::where('id', 2275)->take(4000)->get();

        foreach ($images as $image) {
            if ( ! is_null($image->series_id)) {
                $imagePath = is_null($image->resized_image_path) ? public_path('images/series/' . $image->path) : public_path('images/series/' . $image->resized_image_path)  ;
            } else {
                $imagePath = public_path('images/episodes/' . $image->path);
            }
            try {
                print $imagePath . "\n";
                $optimizerChain->optimize($imagePath);
                $image->image_optimized = Image::$IMAGE_OPTIMIZED;
                $image->save();
            } catch (\Exception $exception) {
                var_dump($exception->getMessage());
            }

        }
    }
}
