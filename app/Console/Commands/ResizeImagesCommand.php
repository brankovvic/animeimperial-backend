<?php

namespace App\Console\Commands;

use App\Image;
use Illuminate\Console\Command;
use Intervention\Image\Facades\Image as ImageResize;
class ResizeImagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resize:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit','256M');

        $images = Image::whereNull('resized_image_path')->where('type', 'poster')->where('series_id', '!=', NULL)->get();

        foreach ($images as $image) {
            echo $image->series_id;
            $imagePath = implode('/', str_split($image->series_id));

            try {

                $image_resize = ImageResize::make(public_path('images/series/' . $image->path));

                $image_resize->resize(383, 483, function($constraint) {
                    $constraint->aspectRatio();
                });
                $resizedImageName = md5(time() . rand()) . '.jpg';

                    $image_resize->save(public_path('images/series/' .$imagePath . '/' . $resizedImageName));

                $image->resized_image_path = $imagePath . '/' . $resizedImageName;

                $image->save();

            } catch (\Exception $exception) {
                echo " " . $exception->getMessage() . "\n";
            }
        }
    }
}
