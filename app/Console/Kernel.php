<?php

namespace App\Console;

use App\Console\Commands\CacheAnime;
use App\Console\Commands\CacheGenrePages;
use App\Console\Commands\CreateSiteMapCommand;
use App\Console\Commands\FetchAnimeWatchLinkCommand;
use App\Console\Commands\FetchRecentEpisodesCommand;
use App\Console\Commands\FixBrokenSeriesDetails;
use App\Console\Commands\ImportEpisodeDetails;
use App\Console\Commands\ImportEpisodeLinks;
use App\Console\Commands\ImportSeriesCommand;
use App\Console\Commands\OptimizeImagesCommand;
use App\Console\Commands\ResizeImagesCommand;
use App\Console\Commands\UpdateEpisodeDetailsCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ImportSeriesCommand::class,
        UpdateEpisodeDetailsCommand::class,
        ResizeImagesCommand::class,
        CreateSiteMapCommand::class,
        OptimizeImagesCommand::class,
        FixBrokenSeriesDetails::class,
        FetchRecentEpisodesCommand::class,
        ImportEpisodeLinks::class,
        ImportEpisodeDetails::class,
        CacheGenrePages::class,
        CacheAnime::class,
        FetchAnimeWatchLinkCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
