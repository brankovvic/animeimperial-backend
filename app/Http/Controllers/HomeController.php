<?php

namespace App\Http\Controllers;

use App\Services\EpisodeService;
use App\Services\SeriesService;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;

class HomeController extends Controller
{
    /**
     * @var SeriesService
     */
    private $seriesService;

    public function __construct(SeriesService $seriesService)
    {
        $this->seriesService = $seriesService;
    }

    public function index(RedisManager $redisManager, EpisodeService $episodeService)
    {
        if ($redisManager->exists('mostViewed')) {
            $mostViewed = json_decode($redisManager->get('mostViewed'));
        } else {
            $mostViewed = $this->seriesService->getMostViewed();
            $redisManager->setex('mostViewed', 86400, json_encode($mostViewed));
        }

        if ($redisManager->exists('topRated')) {
            $topRated = json_decode($redisManager->get('topRated'));
        } else {
            $topRated = $this->seriesService->getTopRated();
            $redisManager->setex('topRated', 86400, json_encode($topRated));
        }

        if ($redisManager->exists('recentAddedAnime')) {
            $recentAdded = json_decode($redisManager->get('recentAddedAnime'));
        } else {
            $recentAdded = $this->seriesService->getRecentAddedAnime();
            $redisManager->setex('recentAddedAnime', 86400, json_encode($recentAdded));
        }

        if ($redisManager->exists('recentAddedEpisodes')) {
            $recentAddedEpisodes = json_decode($redisManager->get('recentAddedEpisodes'));
        } else {
            $recentAddedEpisodes = $episodeService->getNewAddedEpisodes();
            $redisManager->setex('recentAddedEpisodes', 86400, json_encode($recentAddedEpisodes));
        }


        return response()->json([
            'mostViewed'=>$mostViewed,
            'topRated'=>$topRated,
            'recentAdded'=>$recentAdded,
            'episodes'=>$recentAddedEpisodes
        ]);
    }
}
