<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\FetchAnimeWatchLinkJob;
use App\Jobs\FetchTmdbAnimeSeriesJob;
use App\Jobs\ImportTMDBEpisodesJob;
use App\Jobs\ImportTMDBSeasonsJob;
use App\Series;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;

class AdminController extends Controller
{
    public function __construct()
    {

    }

    public function showFormForAddNewAnime()
    {
        return view('admin/addNewAnime');
    }

    public function addNewAnime(Request $request)
    {
        $this->dispatch(new FetchTmdbAnimeSeriesJob($request->get('animeId')));

        return redirect()->back();
    }

    public function showAnime(Series $series)
    {
        return view('admin.seriesInProgress', ['data' => $series->where('import_status', '!=', Series::COMPLETED)->paginate(10)]);
    }

    public function showCrawlerForm(Request $request, Series $series)
    {
        return view('admin.getWatchLinks', ['animeId' => $request->get('animeId'), 'animeTitle' => $series->find($request->get('animeId'))->title]);
    }

    public function changeStatusAction(Request $request)
    {
        switch ($request->get('action')) {
            case 1:
                $this->dispatch(new ImportTMDBSeasonsJob());
                break;
            case 2:
                $this->dispatch(new ImportTMDBEpisodesJob());
                break;
            case 3:
                $this->dispatch(new FetchAnimeWatchLinkJob($request->get('animeId'),
                    $request->get('animeStreamLink'),
                    ($request->has('seasonId') ? $request->get('seasonId') : null)
                    )
                );
                break;
            default:
                break;
        }

        return redirect()->back();
    }

}
