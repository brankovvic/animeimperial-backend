<?php

namespace App\Http\Controllers;

use App\Episode;
use App\Image;
use App\Links;
use App\LinkServer;
use App\Series;
use App\Services\CrawlerImageDownload;
use Illuminate\Http\Request;

class LinksController extends Controller
{
    /**
     * @var Links
     */
    private $links;

    public function __construct(Links $links)
    {

        $this->links = $links;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLinksForVideoLinkExtraction()
    {
        return response()->json($this->links->where('extract_link', true)
            ->where('status', 'offline')
            ->where('status', '!=', 'bad_link')
            ->paginate(5000));
    }

    /**
     * @param Request $request
     */
    public function saveLinksWithOriginalVideoSource(Request $request)
    {
        $data = $request->json()->all();

        foreach ($data as $dataLink) {
            $link = $this->links->where('id', $dataLink['id'])->first();

            if (is_null($link)) continue;

            $link->update($dataLink);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBadLinks(Episode $episode)
    {

        //$episodes = $episode->where('slug', '!=', null)->orderBy('episode_number','ASC')
         //   ->skip(8000)->take(4000)->get();

        //fetch specific episode
        $series = Series::skip(1000)->orderBy('id', 'DESC')->take(1000)->get();
        $fullData = [];

        foreach ($series as $s) {
            $data = [];
            $episodes = $episode->where('slug', '!=', null)->where('series_id', $s->id)->orderBy('episode_number','ASC')->take(4000)->get();


            $link = $this->links;

            $newData = $episodes->filter(function($element) use ($link) {
                return $link->where('episode_id', $element->id)->count() >= 2 ? false : true;

            });

            $data = $this->links->whereIn('episode_id', $newData->pluck('id')->toArray())
                ->orderBy('episode_id', "ASC")->with('episode', 'episode.series')->get();

            if (! empty($data)) {
                array_push($fullData, $data);
            }
        }

        return response()->json($fullData);

        $data = $this->links->where('status', 'bad_link')->with(['episode' => function($query) {
            $query->where('slug', '!=', null);
            $query->orderBy('episode_number','ASC');
            $query->where('series_id', '1215');
        }])->with('episode.series')->take(2000)->get();
        $link = $this->links;



        return response()->json($newCollection);

    }

    /**
     * @param Request $request
     */
    public function updateBadLinks(Request $request)
    {
        $data = $request->json()->all();

        foreach ($data as $dataLink) {
            $this->links->where('id', $dataLink['linkId'])->update(
                [
                    'direct_link' => $dataLink['direct_link'],
                    'link' => $dataLink['link'],
                    'status'=>$dataLink['status']
                ]
            )
            ;
        }

        return response()->json([], 201);
    }

    /**
     * @param Request $request
     * @param Links $links
     *
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function updateBadLinkById(Request $request, Links $links) {
        $data = $request->json()->all();

        if ($data['linkId'] === null) return response()->json([], 201);

        $links->where('id', $data['linkId'])->update(['status'=>Links::$BAD_LINK]);

        return response()->json([], 201);
    }

    /**
     * @param Request $request
     * @param Links $links
     * @param LinkServer $linkServer
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNewLinks(Request $request, Links $links, LinkServer $linkServer)
    {
        $req = $request->json()->all();

        foreach($req as $data) {
            $linkServer = $linkServer->firstOrCreate(['name'=>strtolower($data['service'])]);

            $links->create([
                'server_id'=>$linkServer->id,
                'episode_id'=>$data['episode_id'],
                'status'=>'online',
                'extract_link'=>'0',
                'link'=>$data['link'],
                'direct_link'=>$data['direct_link']
            ]);
        }

        $links->where('id', $req[0]['linkId'])->delete();

        return response()->json([], 201);
    }

    /**
     * @param Episode $episode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSeriesEpisodesWithNoThumb(Episode $episode, Series $series)
    {
        $series = $series->with(['episodes' => function($query) {
            $query->doesntHave('images')->orderBy('episode_number', 'ASC');

        }])->take(1500)->get();

        return response()->json($series);
    }

    /**
     * @param Request $request
     * @param CrawlerImageDownload $crawlerImageDownload
     * @param Episode $episode
     * @param Image $image
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveSeriesEpisodesThumbs(Request $request, CrawlerImageDownload $crawlerImageDownload, Episode $episode, Image $image) {
        $req = $request->json()->all();

        foreach($req as $data) {

            try {

                if ($data['imageUrl'] !== "null") {
                    $episodePath = $crawlerImageDownload->createEpisodeImageDir($data['episodeId']);

                    $imageName = md5(time() . rand()) . '.jpg';

                    $fullImagePath = $crawlerImageDownload->saveImageToDir(
                        $imageName,
                        $episodePath,
                        $data['imageUrl'],
                        Episode::$EPISODE_PATH_NAME
                    );

                    $image->create(
                        [
                            'path' => $fullImagePath,
                            'type' => 'poster',
                            'episode_id' => $data['episodeId'],
                            'image_optimized' => 0
                        ]
                    );
                }
                $e = $episode->where('id', $data['episodeId'])->first();

                if($e->description === null) {
                    $e->description = $data['description'];
                }
                if($e->title === null) {
                    $e->title = $data['title'];
                    $e->slug = str_slug($data['title']);
                }
                $e->save();
            } catch (\Exception $exception) {
                return response()->json([$exception->getMessage()], 401);
            }

            sleep(1);
        }

        return response()->json([], 201);
    }
}
