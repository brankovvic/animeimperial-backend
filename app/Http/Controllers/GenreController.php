<?php

namespace App\Http\Controllers;

use App\Genre;
use App\Series;
use App\SeriesView;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    /**
     * @param $slug
     * @param SeriesView $seriesView
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGenreSeriesBySlug($slug, SeriesView $seriesView, Request $request, Genre $genre)
    {
        $genre = $genre->where('slug', $slug)->first();

        if (is_null($genre)) return response()->json([], 401);
        $genreId = $genre->id;
        //TODO BUG! With genres should be ->whereHas('genres', function($query) use ($genreId) {
        $series = $seriesView->whereHas('genres', function($query) use ($genreId) {
            $query->where('genre_id', $genreId);
        })->sortBy(($request->has('sortBy')) ? $request->get('sortBy') : null)
            ->with('genres')
            ->where('series_completed', Series::COMPLETED)
            ->where('quality', 'HD')
            ->orderBy('viewNumber', 'DESC')
            ->paginate(12);

        return response()->json(['series'=>$series, 'genre'=>$genre]);
    }
}
