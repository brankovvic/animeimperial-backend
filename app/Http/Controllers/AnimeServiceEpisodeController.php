<?php

namespace App\Http\Controllers;

use App\Services\AnimeServices\AnimeEpisodeService;
use Illuminate\Http\Request;

class AnimeServiceEpisodeController extends Controller
{
    /**
     * @var AnimeEpisodeService
     */
    private $animeEpisodeService;

    public function __construct(AnimeEpisodeService $animeEpisodeService)
    {

        $this->animeEpisodeService = $animeEpisodeService;
    }

    /**
     * @param Request $request
     */
    public function saveNewEpisode(Request $request)
    {
        $data = $request->json()->all();

        foreach ($data as $episodeData) {
            $this->animeEpisodeService->saveNewRecentEpisode($episodeData);
        }
    }
}
