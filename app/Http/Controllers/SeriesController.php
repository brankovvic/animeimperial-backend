<?php

namespace App\Http\Controllers;

use App\Episode;
use App\Image;
use App\Series;
use App\SeriesView;
use Illuminate\Http\Request;

class SeriesController extends Controller
{
    /**
     * @param $slug
     * @param SeriesView $seriesView
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSeriesBySlug($slug, SeriesView $seriesView, Request $request, Episode $episode)
    {
        $series = [];

        if ( ! $request->has('page')) {
            $series = $seriesView->with('genres')
                ->where('slug', $slug)
                ->first();

            if (is_null($series)) return response()->json([], 401);
        }

        $episodes = $episode->where('slug', '!=', null)->whereHas('series', function ($query) use ($slug) {
            $query->where('slug', $slug);
        })->orderBy('episode_number', 'ASC')->with('images')->paginate(9);

        return response()->json(['series'=>$series,'episodes'=>$episodes], 200);
    }

    public function getBrowseSeries(Request $request, SeriesView $seriesView)
    {
        $series = $seriesView->with('genres')
            ->where('series_completed', Series::COMPLETED)
            ->sortBy($request->get('sortBy'))->paginate(6);

        return response()->json($series, (is_null($series) ? 401 : 201));
    }

    public function getNewAddedSeries(SeriesView $seriesView) {
        $series = $seriesView->with('genres')
            ->where('series_completed', Series::COMPLETED)
            ->orderBy('created_at', 'DESC')
            ->paginate(12);

        return response()->json($series, (is_null($series) ? 401 : 201));
    }

    /**
     * @param $animeSlug
     * @param Series $series
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function animeExists($animeSlug, Series $series) {

        $animeExists = $series->where('slug', $animeSlug)->count();

        return response()->json(['exists' => ($animeExists > 0) ? true : false]);
    }
}
