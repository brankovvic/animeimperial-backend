<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\ContactRequest;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * @var Contact
     */
    private $contact;

    public function __construct(Contact $contact)
    {

        $this->contact = $contact;
    }

    public function index(ContactRequest $contactRequest)
    {
        try{
            $this->contact->create($contactRequest->except(['submitted']));
            return response()->json([], 201);
        } catch (\Exception $exception) {
            return response()->json([], 401);
        }
    }
}
