<?php

namespace App\Http\Controllers;

use App\Services\EpisodeVotesService;
use App\Services\VotesService;
use Illuminate\Http\Request;

class VoteController extends Controller
{
    /**
     * @var EpisodeVotesService
     */
    private $votesService;

    public function __construct(EpisodeVotesService $votesService)
    {
        $this->votesService = $votesService;
    }

    /**
     * @param Request $request
     */
    public function like(Request $request) {
        $this->votesService->like($request->get('episode_id'), $request->getClientIp());
    }

    /**
     * @param Request $request
     */
    public function dislike(Request $request)
    {
        $this->votesService->dislike($request->get('episode_id'), $request->getClientIp());
    }
}
