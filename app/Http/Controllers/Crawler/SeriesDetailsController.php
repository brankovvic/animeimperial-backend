<?php

namespace App\Http\Controllers\Crawler;

use App\CrawlerEpisodeLink;
use App\CrawlerLink;
use App\Episode;
use App\Genre;
use App\Links;
use App\LinkServer;
use App\Series;
use App\SeriesGenre;
use App\SeriesStatistic;
use App\SeriesUserView;
use App\Studio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SeriesDetailsController extends Controller
{
    /**
     * @var CrawlerLink
     */
    private $crawlerLink;

    public function __construct(CrawlerLink $crawlerLink)
    {

        $this->crawlerLink = $crawlerLink;
    }

    public function fetchCrawlerLinks()
    {

    }

    /**
     * @param Request $request
     */
    public function saveCrawlerLink(Request $request)
    {
        $data = $request->json()->all();

        $data['title'] = trim(str_replace('(Dub)', '', $data['title']));

        try {
            $this->crawlerLink->create($data);
            return response()->json([], 201);
        } catch (\Exception $exception) {
            return response()->json([], 201);
        }
    }

    /**
     * @param Request $request
     */
    public function createEpisodeDetails(Request $request)
    {
        $data = $request->json()->all();
        try {

            DB::beginTransaction();
        $genreIds = [];

        foreach ($data['genres'] as $genre) {
            $genre = Genre::firstOrCreate([
                'name'=> $genre,
                'slug'=>str_slug($genre, '-')
            ]);
            array_push($genreIds, $genre->id);
        }

        $studio = Studio::create([
            'name'=> $data['studio'],
            'slug'=>str_slug($data['studio'], '-')
        ]);

        $series = Series::firstOrCreate([
            'title'=> $data['title'],
            'slug'=>str_slug($data['title'], '-'),
            'studio_id'=>$studio->id,
            'aired'=>$data['aired'],
            'status'=>$data['status'],
            'premiere'=>$data['premiere'],
            'quality'=>$data['quality'],
            'description'=>$data['description']
        ]);
        //TODO Remove Default Data
        foreach ($data['episodes'] as $episodeIndex=>$episodeData) {
            $episode = Episode::firstOrCreate([
                'episode_number'=>$episodeIndex,
                'series_id'=>$series->id,
                'title'=>'Episode ' . $episodeIndex,
                'slug'=>'episode-' . trim($episodeIndex)
            ]);
            foreach ($episodeData["episodeLinks"] as $index=>$linksData) {
                $linkServer = LinkServer::firstOrCreate([
                    'name'=>$linksData["server"]
                ]);

                Links::firstOrCreate([
                    'server_id'=>$linkServer->id,
                    'episode_id'=>$episode->id,
                    'link'=>$linksData['link'],
                    'status'=>Links::$ONLINE
                ]);

                CrawlerEpisodeLink::firstOrCreate([
                    'episode_id'=> $episode->id,
                    'link'=> $episodeData["episodeId"]
                ]);
            }
        }

        foreach ($genreIds as $genreDatabaseId) {
            SeriesGenre::firstOrCreate([
                'genre_id'=>$genreDatabaseId,
                'series_id'=>$series->id
            ]);
        }

        SeriesUserView::firstOrCreate([
            'series_id'=>$series->id,
            'view_number'=>intval(str_replace( ",", "",$data['views']))
        ]);

        SeriesStatistic::firstOrCreate([
            'series_id'=>$series->id,
            'rating_number'=>floatval($data['ratingNumber']),
            'rating_count'=>intval($data['ratingCount']),
            'score_number'=>floatval($data['scoreNumber']),
            'score_count'=>intval($data['scoreCount'])
        ]);

        DB::commit();
            return response()->json([], 201);
        } catch (\Exception $exception) {
            DB::rollBack();

            return response()->json($exception->getMessage(), 401);
        }
    }

    public function getSeriesLinks(CrawlerLink $crawlerLink)
    {
        return response()->json($crawlerLink->where('completed', CrawlerLink::INITIAL)
            ->where('completed','!=', CrawlerLink::COMPLETED)
            ->where('completed', '!=', CrawlerLink::BAD)
            ->paginate(1400));
    }
}
