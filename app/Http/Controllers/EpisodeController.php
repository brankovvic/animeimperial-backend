<?php

namespace App\Http\Controllers;

use App\Episode;
use App\EpisodeRating;
use App\EpisodeView;
use function App\Helpers\isURLValid;
use function App\Helpers\makeGetRequest;
use App\Links;
use App\LinkServer;
use App\PageView;
use App\Series;
use App\Services\EpisodeService;
use App\Services\EpisodeVotesService;
use App\Services\VotesService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;
use Symfony\Component\DomCrawler\Link;

class EpisodeController extends Controller
{
    /**
     * @var EpisodeService
     */
    private $episodeService;

    public function __construct(EpisodeService $episodeService)
    {

        $this->episodeService = $episodeService;
    }

    public function getEpisodeDetails(
        $slug,
        $seriesSlug,
        Episode $episode,
        EpisodeVotesService $episodeVotesService,
        RedisManager $redisManager,
        PageView $pageView,
        Request $request
    ) {

        $episodeQuery = $episode->with('images', 'series', 'series.genres')->with(
            [
                'links' => function ($query) {
                    return $query->where('status', 'online');
                },
            ]
        )->where('slug', $slug)
        ;

        if ($seriesSlug !== "null") {
            $episodeQuery->whereHas(
                'series',
                function ($query) use ($seriesSlug) {
                    $query->where('slug', $seriesSlug);
                }
            );
        };

        $activeEpisode = $episodeQuery->first();

        if (is_null($activeEpisode)) {
            return response()->json([], 401);
        }

        $activeEpisode->ratingInfo = new \stdClass();

        if ($redisManager->hexists('vote', $activeEpisode->id)) {
            $voteResult = json_decode($redisManager->hget('vote', $activeEpisode->id));

            $activeEpisode->ratingInfo->likesCount = $voteResult['likesCount'];
            $activeEpisode->ratingInfo->dislikeCount = $voteResult['dislikeCount'];
            $activeEpisode->ratingInfo->likesPerCent = $voteResult['likesPerCent'];
            $activeEpisode->ratingInfo->dislikePerCent = $voteResult['dislikePerCent'];
            $activeEpisode->ratingInfo->totalCount = $voteResult['totalCount'];
        } else {
            $activeEpisode->ratingInfo->likesCount = $episodeVotesService->likes(
                $activeEpisode->id
            );
            $activeEpisode->ratingInfo->dislikeCount = $episodeVotesService->dislikes(
                $activeEpisode->id
            );
            if ($activeEpisode->ratingInfo->likesCount === 0
                || $activeEpisode->ratingInfo->dislikeCount === 0) {
                if ($activeEpisode->ratingInfo->likesCount === 0) {
                    $activeEpisode->ratingInfo->likesPerCent = 0;
                    $activeEpisode->ratingInfo->dislikePerCent = 100;
                } else {
                    $activeEpisode->ratingInfo->likesPerCent = 100;
                    $activeEpisode->ratingInfo->dislikePerCent = 0;
                }
            } else {
                $activeEpisode->ratingInfo->likesPerCent = round(
                    $activeEpisode->likesCount / $activeEpisode->dislikeCount
                );
                $activeEpisode->ratingInfo->dislikePerCent = round(
                    $activeEpisode->dislikeCount / $activeEpisode->likesCount
                );
            }

            $activeEpisode->ratingInfo->totalCount = $episodeVotesService->votesCount(
                $activeEpisode->id
            );

            $activeEpisode->ratingInfo->pageViews = number_format(
                $pageView->where('episode_id', $activeEpisode->id)->count()
            );
            $activeEpisode->userVoted = $episodeVotesService->userVoted(
                $activeEpisode->id,
                $request->getClientIp()
            );

            $redisManager->hset(
                'episodeVotes',
                $activeEpisode->id,
                json_encode(
                    [
                        'likesCount' => $activeEpisode->ratingInfo->likesCount,
                        'dislikeCount' => $activeEpisode->ratingInfo->dislikeCount,
                        'likesPerCent' => $activeEpisode->ratingInfo->likesPerCent,
                        'dislikePerCent' => $activeEpisode->ratingInfo->dislikePerCent,
                        'totalCount' => $activeEpisode->ratingInfo->totalCount,
                    ]
                )
            );
            $redisManager->expire('episodeVotes', EpisodeRating::$REDIS_CACHE_TIME);
        }

        return response()->json(
            [
                'activeEpisode' => $activeEpisode,
                'watchEpisodeStream' => $this->episodeService->getWatchEpisodeStream(
                    $activeEpisode
                ),
            ]
        );
    }

    /**
     * Get Stream Url And Using Service Extracts Direct Url
     * @param $episodeId
     * @param Links $links
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchEmbeddedUrlForEpisode($episodeId, Links $links)
    {
        $links = $links->where('episode_id', $episodeId)->with('server')
            ->where('server_id', '!=', 4)->orderBy('server_id', 'DESC')->get();

        if ($links->count() === 0) return response()->json(['link' => null]);

        return response()->json(['directLink'=>true, 'link' => "https://redirector.googlevideo.com/videoplayback?id=299a1f4773b4858a&amp;itag=18&amp;source=picasa&amp;begin=0&amp;requiressl=yes&amp;mm=30&amp;mn=sn-5hne6nsz&amp;ms=nxu&amp;mv=u&amp;pl=24&amp;sc=yes&amp;ei=DeDTXKP3B5Xh1wLm6I_4BQ&amp;susc=ph&amp;app=fife&amp;mime=video/mp4&amp;dur=1364.659&amp;lmt=1552375056891406&amp;mt=1557389078&amp;ipbits=0&amp;keepalive=yes&amp;ratebypass=yes&amp;ip=134.19.181.231&amp;expire=1557396525&amp;sparams=ip,ipbits,expire,id,itag,source,requiressl,mm,mn,ms,mv,pl,sc,ei,susc,app,mime,dur,lmt&amp;signature=66BA253A444330CACE39387A87E3B915544F1942B695C47C76D4D986B9967BD3.A78C2C218334CA635612CB33162352951243FD967DD41CD180F02D068AE1E5C3&amp;key=us0"]);

        foreach ($links as $link) {
            if (isURLValid($link['link'])) {

                $response = makeGetRequest(env('SERVICES_ADDRESS') . 'direct/link/get',
                    ['link'=>$link['link'], 'streamType'=>$link->server->name]
                );

                if($response === false) {
                    $response = makeGetRequest(env('SERVICES_ADDRESS'). 'direct/link/get',
                        ['link'=>$link['link'], 'streamType'=>$link->server->name]
                    );
                }

                if ($response['directLink'] === null) {
                    continue;
                }

                $link->updateDirectLink($response['directLink']);

                return response()->json(['link' => $response['directLink'],'directLink'=>true]);
            }
        }

        return response()->json(['link' => null]);
    }

    /**
     * @param EpisodeService $episodeService
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNewEpisodes(EpisodeService $episodeService) {
        return response()->json(['episodes'=>$episodeService->getNewAddedEpisodes()]);
    }

    /**
     * @param $animeSlug
     * @param $episodeSlug
     * @param Episode $episode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function episodeExists($episodeSlug, $animeSlug, Episode $episode) {
        $episodeCount = $episode->where('slug', $episodeSlug)->whereHas('series', function($query) use ($animeSlug) {
           $query->where('slug', $animeSlug);
        })->count();

        return response()->json(['exists'=>($episodeCount > 0) ? true : false]);
    }
}
