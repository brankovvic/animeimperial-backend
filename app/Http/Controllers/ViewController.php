<?php

namespace App\Http\Controllers;

use App\EpisodeView;
use App\PageView;
use App\Series;
use Illuminate\Http\Request;

class ViewController extends Controller
{
    /**
     * @var PageView
     */
    private $pageView;

    public function __construct(PageView $pageView)
    {

        $this->pageView = $pageView;
    }

    public function episodePageView($episodeSlug, $seriesSlug, Request $request, Series $series)
    {
        $series = $series->where('slug', $seriesSlug)->whereHas('episodes', function($query) use ($episodeSlug) {
            $query->where('slug', $episodeSlug);
        })->first();

        if ( $series === null) return response()->json(201);

        $episode = $series->getEpisodeBySlug($episodeSlug)->first();

        $this->pageView->firstOrCreate([
           'episode_id'=> $episode->id,
           'client_ip'=>$request->getClientIp()
        ]);

        return response()->json([], 201);
    }
}
