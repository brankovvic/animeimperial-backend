<?php

namespace App\Http\Controllers;

use App\Series;
use App\SeriesView;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * @param $search
     * @param SeriesView $seriesView
     *
     * @return JsonResponse
     */
    public function search($search, SeriesView $seriesView): JsonResponse
    {
        return response()->json($seriesView->with('genres')->where('movie', '!=', Series::MOVIE)
            ->where('title', 'LIKE', "%$search%")
            ->whereHas('episodes', function($query) {
                $query->where('title', '!=',null);
                $query->where('slug', '!=', null);
            })->limit(6)->get());
    }
}
