<?php

namespace App\Jobs;

use App\Image;
use App\Season;
use App\Series;
use App\Services\CrawlerImageDownload;
use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ImportTMDBSeasonsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $series = Series::where('import_status', Series::SEASON_IMPORT)->limit(5)->get();
        $imageSaving = new CrawlerImageDownload();

        try {
            DB::beginTransaction();
            foreach ($series as $s) {
                $data = tmdb()->getTVShow($s->tmdb_id);
                foreach ($data->getSeasons() as $season) {

                    //SKIP Specials
                    if ($season->getSeasonNumber() == 0) continue;

                    if (Carbon::now()->lessThan(new Carbon($season->getAirDate()))) {
                        continue;
                    }

                    $cratedSeason = Season::create([
                        'tmdb_id'=>$season->getID(),
                        'name'=>$season->getName(),
                        'description'=>$season->get('overview'),
                        'aired'=>$season->getAirDate(),
                        'episode_count'=>$season->get('episode_count'),
                        'season_number'=>$season->getSeasonNumber(),
                        'series_id'=>$s->id
                    ]);
                    if($season->getPoster() !== null) {
                        $imagePath = $imageSaving->createSeasonImageDir($cratedSeason->id);
                        $imageName = md5(time() . rand()) . '.jpg';
                        $fullPath = $imageSaving->saveImageToDir($imageName, $imagePath, $imageSaving->baseImagePath . $season->getPoster(), 'season');
                        Image::create([
                            'path'=>$fullPath,
                            'type'=>'poster',
                            'season_id'=>$cratedSeason->id
                        ]);
                    }
                }
                $s->import_status = Series::EPISODE_IMPORT;
                $s->save();
            }

            DB::commit();
        } catch (\Exception $exception)
        {
            DB::rollBack();
            Log::info($exception->getMessage());
        }
    }
}
