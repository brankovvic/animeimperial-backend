<?php

namespace App\Jobs;

use App\Episode;
use App\Image;
use App\Season;
use App\Series;
use App\Services\CrawlerImageDownload;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ImportTMDBEpisodesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $series = Series::where('import_status', Series::EPISODE_IMPORT)->limit(5)->get();
        $imageSaving = new CrawlerImageDownload();
        try {
            DB::beginTransaction();
            foreach ($series as $s) {
                $seasons = Season::where('series_id', $s->id)->get();

                foreach ($seasons as $season) {
                    for ($i=1;$i<=$season->episode_count;$i++) {
                        sleep(5);
                        $episodeInfo = tmdb()->getEpisode($s->tmdb_id, $season->season_number, $i);

                        if (Carbon::now()->lessThan(new Carbon($episodeInfo->getAirDate()))) {
                            continue;
                        }

                        $episode = Episode::create([
                            'series_id'=>$s->id,
                            'season_id'=>$season->id,
                            'episode_number'=>$i,
                            'slug'=>str_slug($episodeInfo->getName(), '-'),
                            'title'=>$episodeInfo->getName(),
                            'description'=>$episodeInfo->getOverview(),
                            'aired'=>$episodeInfo->getAirDate()
                        ]);

                        if($episodeInfo->get('still_path') !== null) {
                            $imagePath = $imageSaving->createEpisodeImageDir($episode->id);
                            $imageName = md5(time() . rand()) . '.jpg';
                            $fullPath = $imageSaving->saveImageToDir($imageName, $imagePath, $imageSaving->baseImagePath . $episodeInfo->get('still_path'), 'episodes');
                            Image::create([
                                'path'=>$fullPath,
                                'type'=>'poster',
                                'episode_id'=>$episode->id
                            ]);
                        }
                    }
                }
                $s->import_status = Series::WATCH_LINKS_IMPORT;
                $s->save();
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::info($exception->getMessage());
        }
    }
}
