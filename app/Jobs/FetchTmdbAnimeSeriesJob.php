<?php

namespace App\Jobs;

use App\Genre;
use App\Image;
use App\Series;
use App\SeriesGenre;
use App\SeriesStatistic;
use App\Services\CrawlerImageDownload;
use App\Studio;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FetchTmdbAnimeSeriesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var int
     */
    private $animeId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $animeId)
    {
        //
        $this->animeId = $animeId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = tmdb()->getTVShow($this->animeId);

        try {
            DB::beginTransaction();
            $genreIds = [];
            foreach ($data->get('genres') as $genre) {
                $genre = Genre::firstOrCreate(
                    [
                        'name' => $genre['name'],
                        'slug' => str_slug($genre['name'], '-'),
                    ]
                );
                array_push($genreIds, $genre->id);
            }

            $studio = Studio::create([
                'name'=> $data->get('networks')[0]['name'],
                'slug'=>str_slug($data->get('networks')[0]['name'], '-')
            ]);

            $series = Series::firstOrCreate([
                'title'=> $data->getName(),
                'slug'=>str_slug($data->getName(), '-'),
                'studio_id'=>$studio->id,
                'aired'=>$data->get('first_air_date'),
                'status'=>$data->get('status'),
                'premiere'=>$data->get('first_air_date'),
                'description'=>$data->getOverview(),
                'tmdb_id'=>$data->getID(),
                'import_status'=>Series::SEASON_IMPORT

            ]);

            $imageSaving = new CrawlerImageDownload();

            $path = $imageSaving->createSeriesImageDir($series->id);
            $imageName = md5(time() . rand()) . '.jpg';
            $imagePath = $imageSaving->saveImageToDir($imageName, $path, $imageSaving->baseImagePath . $data->getPoster(), 'series');

            Image::create([
                'path'=>$imagePath,
                'type'=>'poster',
                'series_id'=>$series->id
            ]);

            if ($data->get('backdrop_path') !== null) {
                $path = $imageSaving->createSeriesImageDir($series->id);
                $imageName = md5(time() . rand()) . '.jpg';
                $imagePath = $imageSaving->saveImageToDir($imageName, $path, $imageSaving->baseImagePath . $data->get('backdrop_path'), 'series');

                Image::create([
                    'path'=>$imagePath,
                    'type'=>'cover',
                    'series_id'=>$series->id
                ]);
            }

            foreach ($genreIds as $genreDatabaseId) {
                SeriesGenre::firstOrCreate(
                    [
                        'genre_id' => $genreDatabaseId,
                        'series_id' => $series->id,
                    ]
                );
            }
            SeriesStatistic::firstOrCreate(
                [
                    'series_id' => $series->id,
                    'rating_number' => floatval($data->getVoteAverage()),
                    'rating_count' => intval($data->getVoteCount()),
                    'score_number' => 0,
                    'score_count' => 0,
                ]
            );
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::critical($exception->getMessage());
        }
    }
}
