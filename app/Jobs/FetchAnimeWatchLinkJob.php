<?php

namespace App\Jobs;

use App\Episode;
use App\Links;
use App\Season;
use App\Series;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FetchAnimeWatchLinkJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var int
     */
    private $animeId;
    /**
     * @var string
     */
    private $link;
    /**
     * @var int
     */
    private $seasonId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $animeId, string $link, int $seasonId = null)
    {
        //
        $this->animeId = $animeId;
        $this->link = $link;
        $this->seasonId = $seasonId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $a = Episode::where('series_id', $this->animeId);

        if ( ! is_null($this->seasonId)) {
            $season = Season::where('series_id', $this->animeId)->where('season_number', $this->seasonId)->first();
            $a->where('season_id', $season->id);
        }

        $animeEpisodes = $a->orderBy('id', 'ASC')->get();


        try{
            DB::beginTransaction();
            $client = new Client();
            $res = $client->request('GET', env('ANIME_WATCH_LINK_SERVICE'), [
                'query' => ['link' => $this->link]
            ]);
            if($res->getStatusCode() !== 200) {
                throw new \Exception("Bad Http Request");
            }
            $data = json_decode($res->getBody(), true)['data'];

            for($i=1;$i<=count($animeEpisodes); $i++) {

                if ( ! array_key_exists($i, $data)) continue;

                Links::create([
                    'server_id'=>1,
                    'episode_id'=>$animeEpisodes[$i-1]->id,
                    'link'=>$data[$i],
                    'status'=>Links::$ONLINE
                ]);
            }
            Series::where('id', $this->animeId)->update(['import_status'=>Series::COMPLETED]);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::critical($exception->getMessage());
        }
    }
}
