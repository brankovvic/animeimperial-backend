<?php

namespace App\Jobs;

use App\CrawlerEpisodeLink;
use App\Episode;
use App\Genre;
use function App\Helpers\makeGetRequest;
use App\Image;
use App\Links;
use App\LinkServer;
use App\Series;
use App\SeriesGenre;
use App\SeriesStatistic;
use App\SeriesUserView;
use App\Services\CrawlerImageDownload;
use App\Studio;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AddNewSeries implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $seriesInfo;
    private $crawlerImageDownload;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($seriesInfo)
    {
        $this->seriesInfo = $seriesInfo;
        $this->crawlerImageDownload = new CrawlerImageDownload();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = makeGetRequest(env('SERVICES_ADDRESS') . 'anime/series/info/get', [
                'title'=>$this->seriesInfo['seriesTitle'],
                'slug'=>str_slug($this->seriesInfo['seriesTitle'], '-')
            ]);

        if (empty($data)) return;

        try{
            DB::beginTransaction();
            $studio = Studio::firstOrCreate([
                'name'=>$data['network'],
                'slug'=>str_slug($data['network'], '-')
            ]);

            $series = Series::firstOrCreate([
                'title'=>$this->seriesInfo['seriesTitle'],
                'slug'=>str_slug($this->seriesInfo['seriesTitle'], '-'),
                'studio_id'=>$studio->id,
                'aired'=>($data['firstAired'] !== "") ? $data['firstAired'] : $data['added'],
                'status'=>$data['status'],
                'premiere'=>($data['firstAired'] !== "") ? $data['firstAired'] : $data['added'],
                'quality'=>'HD',
                'description'=>$data['overview'],
                'imdb_id'=>$data['imdbId'],
                'tmdb_id'=>$data['id'],
                'tmdb_name'=>$data['seriesName'],
                'series_completed'=>1
            ]);

            $genreIds = [];
            foreach ($data['genre'] as $genre) {
                $genre = Genre::firstOrCreate([
                    'name'=> $genre,
                    'slug'=>str_slug($genre, '-')
                ]);
                array_push($genreIds, $genre->id);
            }

            foreach ($genreIds as $genreDatabaseId) {
                SeriesGenre::firstOrCreate([
                    'genre_id'=>$genreDatabaseId,
                    'series_id'=>$series->id
                ]);
            }

            SeriesUserView::firstOrCreate([
                'series_id'=>$series->id,
                'view_number'=>$data['siteRatingCount']
            ]);

            SeriesStatistic::firstOrCreate([
                'series_id'=>$series->id,
                'rating_number'=>$data['siteRating'],
                'rating_count'=>$data['siteRatingCount'],
                'score_number'=>0,
                'score_count'=>0
            ]);


            foreach ($data['episodes'] as $episodeData) {
                $firstAired = new Carbon($episodeData['firstAired']);

                if ($firstAired > new Carbon()) continue;

                if($episodeData['airedEpisodeNumber'] > $this->seriesInfo['episodeNumber']) break;

                $episode = Episode::firstOrCreate([
                    'episode_number'=>$episodeData['airedEpisodeNumber'],
                    'title'=>$episodeData['episodeName'],
                    'slug'=>str_slug($episodeData['episodeName'], '-'),
                    'description'=>$episodeData['overview'],
                    'series_id'=>$series->id,
                    'aired'=>($episodeData['firstAired'] !== "") ? $episodeData['firstAired'] : Carbon::now()
                ]);
                try {
                    $episodePath = $this->crawlerImageDownload->createEpisodeImageDir($episode->id);

                    $imageName = md5(time() . rand()) . '.jpg';

                    if ($episodeData['filename'] === null || $episodeData === "") {
                        continue;
                    }

                    $fullImagePath = $this->crawlerImageDownload->saveImageToDir(
                        $imageName,
                        $episodePath,
                        $episodeData['filename'],
                        Episode::$EPISODE_PATH_NAME
                    );

                    Image::create(
                        [
                            'path' => $fullImagePath,
                            'type' => 'poster',
                            'episode_id' => $episode->id,
                            'image_optimized' => 0
                        ]
                    );
                } catch (\Exception $exception) {
                    continue;
                }
            }

            $response = makeGetRequest(env('SERVICES_ADDRESS') . 'anime/series/episodes/get', [
                'link'=>$this->seriesInfo['seriesLink'],
            ]);

            if (empty($response)) return;

            foreach ($response as $episodeNumber=>$episodeLinks) {

                $episode = Episode::where('series_id', $series->id)->where('episode_number', $episodeNumber)->first();

                if(is_null($episode)) continue;

                foreach ($episodeLinks as $link) {
                    $linkServer = LinkServer::firstOrCreate([
                        'name'=>strtolower($link['service'])
                    ]);

                    Links::firstOrCreate([
                        'server_id'=>$linkServer->id,
                        'episode_id'=>$episode->id,
                        'link'=>$link['link'],
                        'status'=>Links::$ONLINE
                    ]);
                }
            }

            foreach ($data['images']['cover'] as $cover) {
                try {
                    $episodePath = $this->crawlerImageDownload->createSeriesImageDir($series->id);

                    $imageName = md5(time() . rand()) . '.jpg';

                    if($cover['fileName'] === null || $cover['fileName'] === "") continue;

                    $fullImagePath = $this->crawlerImageDownload->saveImageToDir(
                        $imageName,
                        $episodePath,
                        $cover['fileName'],
                        Series::SERIES_PATH_NAME
                    );

                    Image::create(
                        [
                            'path' => $fullImagePath,
                            'type' => 'cover',
                            'series_id' => $series->id,
                            'image_optimized' => 0
                        ]
                    );
                } catch (\Exception $exception) {
                    continue;
                }
            }

            foreach ($data['images']['poster'] as $poster) {
                try{
                    $episodePath = $this->crawlerImageDownload->createSeriesImageDir($series->id);

                    $imageName = md5(time() . rand()) . '.jpg';

                    if($poster['fileName'] === null || $poster['fileName'] === "") continue;

                    $fullImagePath = $this->crawlerImageDownload->saveImageToDir(
                        $imageName,
                        $episodePath,
                        $poster['fileName'],
                        Series::SERIES_PATH_NAME
                    );

                    Image::create(
                        [
                            'path' => $fullImagePath,
                            'type' => 'poster',
                            'series_id' => $series->id,
                            'image_optimized' => 0
                        ]
                    );
                } catch (\Exception $exception) {
                    continue;
                }
            }

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::info($exception->getMessage());
        }
    }
}
