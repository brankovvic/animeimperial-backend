<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkServer extends Model
{
    static $STREAMANGO = 4;

    protected $guarded = ['id'];
}
