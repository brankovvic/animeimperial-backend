<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Links extends Model
{
    static $BAD_LINK = "bad_link";

    static $ONLINE = "online";

    static $STREAM_SERVER = 4;

    protected $guarded = ['id'];

    public function crawlerLink()
    {
        return $this->hasOne(CrawlerEpisodeLink::class, 'episode_id', 'episode_id');
    }

    public function episode()
    {
        return $this->hasOne(Episode::class, 'id', 'episode_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function server() {
        return $this->belongsTo(LinkServer::class);
    }

    /**
     * @param string $directLink
     *
     * @return bool
     */
    public function updateDirectLink(string $directLink) {
        $this->direct_link = $directLink;
        $this->status = self::$ONLINE;
        return $this->save();
    }
}
