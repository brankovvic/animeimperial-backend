<?php namespace App\Helpers;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;

function isURLValid($URL): bool{
    $headers = @get_headers($URL);
    preg_match("/ [45][0-9]{2} /", (string)$headers[0] , $match);
    return count($match) === 0;
}

function makeGetRequest(string $url, array $parameters) {
    try{
        $client = new Client();
        $res = $client->request('GET', $url, ['query' => $parameters]);

        if ($res->getStatusCode() >= 400) {
            return null;
        }

        if($res->getHeader('content-type') !== false && $res->getHeader('content-type')[0] === "application/json") {
            return json_decode($res->getBody(), true);
        }

        return null;
    } catch (ServerException $exception) {
        return false;
    } catch (ConnectException $exception) {
        return false;
    }


}
