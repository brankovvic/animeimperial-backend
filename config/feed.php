<?php

return [
    'feeds' => [
        'popular' => [
            /*
             * Here you can specify which class and method will return
             * the items that should appear in the feed. For example:
             * 'App\Model@getAllFeedItems'
             *
             * You can also pass an argument to that method:
             * ['App\Model@getAllFeedItems', 'argument']
             */
            'items' => 'App\SeriesView@getFeedItems',

            /*
             * The feed will be available on this url.
             */
            'url' => '/feed',

            'title' => 'Popular anime on AnimeImperial.com',

            /*
             * The view that will render the feed.
             */
            'view' => 'vendor.feed.feed',
        ],
    ],
];
