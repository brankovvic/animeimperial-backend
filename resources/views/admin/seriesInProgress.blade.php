@extends('index')

@section('content')
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Slug</th>
            <th scope="col">TmdbId</th>
            <th scope="col">Status</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $anime)
        <tr>
            <th scope="row">{{$anime->id}}</th>
            <td>{{$anime->title}}</td>
            <td>{{$anime->slug}}</td>
            <td>{{$anime->tmdb_id}}</td>
            <td>{{$anime->getSeriesStatus($anime->import_status)}}</td>
            <td>
                <form method="post" action="{{route('changeStatusAction')}}">
                    {{csrf_field()}}
                    @if($anime->import_status !== \App\Series::WATCH_LINKS_IMPORT)
                        <input type="hidden" name="action" value="{{$anime->import_status}}">
                        <button type="submit" class="btn btn-primary mb-2">Submit</button>
                    @else
                        <a href="{{route('showCrawlerForm') ."?animeId=" . $anime->id}}" class="btn btn-primary mb-2">Update Links</a>
                    @endif
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection