@extends('index')

@section('content')
    <div class="container-fluid" style="width: 50%">
    <form method="post" action="{{route('addNewAnime')}}">
        {{csrf_field()}}
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Anime TMDB ID</label>
            <div class="col-sm-10">
                <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="email@example.com">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="animeId" id="tmdb" placeholder="Anime ID">
            </div>
        </div>
        <button type="submit" class="btn btn-primary mb-2">Submit</button>
    </form>
    </div>
@endsection