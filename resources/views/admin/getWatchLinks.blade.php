@extends('index')

@section('content')
    <div class="container-fluid" style="width: 50%">
        <form method="post" action="{{route('changeStatusAction')}}">
            {{csrf_field()}}
            <label for="staticEmail" class="col-sm-2 col-form-label">{{$animeTitle}}</label>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Animestreams URL</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="animeStreamLink" id="tmdb" placeholder="Animestreams URL" />
                </div>

                <label for="staticEmail" class="col-sm-2 col-form-label">Season Id</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="seasonId" id="seasonId" placeholder="SeasonID" />
                </div>
            </div>
            <input type="hidden" value="{{$animeId}}" name="animeId">
            <input type="hidden" value="3" name="action">
            <button type="submit" class="btn btn-primary mb-2">Submit</button>
        </form>
    </div>
@endsection