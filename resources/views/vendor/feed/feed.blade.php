<?=
    /* Using an echo tag here so the `<? ... ?>` won't get parsed as short tags */
    '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL
?>
<feed xmlns="http://www.w3.org/2005/Atom">
    @foreach($meta as $key => $metaItem)
        @if($key === 'link')
            <{{ $key }} href="https://www.animeimperial.com"></{{ $key }}>
        @elseif($key=== 'id')
            <{{ $key }} href="https://www.animeimperial.com/feed"></{{ $key }}>
        @elseif($key === 'title')
            <{{ $key }}><![CDATA[{{ $metaItem }}]]></{{ $key }}>
        @else
            <{{ $key }}>{{ $metaItem }}</{{ $key }}>
        @endif
    @endforeach
    @foreach($items as $item)
        <entry>
            <title><![CDATA[{{ $item->title }}]]></title>
            <link rel="alternate" href="https://www.animeimperial.com/{{ $item->link }}" />

            <summary type="html">
                <![CDATA[<img align="left" title="{{$item->title}}" src="{{ $item->image }}" />]]>
               {!! $item->summary !!}
            </summary>
            <image>
                <url>{{$item->image}}</url>
            </image>
            <updated>{{ $item->updated->toAtomString() }}</updated>
        </entry>
    @endforeach
</feed>
