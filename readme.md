Setup Application
----------------------------
Verify that you have docker and docker-compose up and running before continuing

.. code-block:: console
    
    $ https://www.docker.com/get-started

After cloning the project use docker and build project

.. code-block:: console

    $ docker-compose build

After docker finish with build process bring the docker up

.. code-block:: console

    $ docker-compose up --build

After docker is up you should see mysql output into the terminal, also you can check with

.. code-block:: console

    $ docker container list

If everything went ok without any errors now you need to install composer dependencies with

Note: "survey-backend_php_1" survey-backend is name of the folder where you clone your project
if you used a different name for project use that instead and just append _php_1 at the end

.. code-block:: console

    $ docker exec -it survey-backend_php_1 composer install

After composer finish installing copy the .env.example to .env with following command

.. code-block:: console

    $ docker exec -it survey-backend_php_1 cp .env.example .env

Run command to generate application key

.. code-block:: console

    $ docker exec -it survey-backend_php_1 php artisan key:generate

Run migrations to migrate tables:

.. code-block:: console

    $ docker exec -it survey-backend_php_1 php artisan migrate

After Migrations Is complete change the permisions to storage folder and inside storage folder make 
next folder structure:

.. code-block:: console

    $ mkdir -p storage/framework/views
    $ mkdir -p storage/framework/sessions
    $ mkdir -p storage/framework/cache
    $ chmod -R 777 storage/
    $ chmord -R 777 public/files    
Execute the tests to insure that setup is done correctly

.. code-block:: console

    $ docker exec -it survey-backend_php_1 ./vendor/bin/phpunit

To Create Api Documentation Run Command and visit the url http://127.0.0.1:8083/api/documentation

.. code-block:: console

    $ docker exec -it survey-backend_php_1 php artisan l5-swagger:generate
    
You can execute docker commands via, just append what you want to the end or you create alias

.. code-block:: console

    $ docker exec -it survey-backend_php_1 

To access the web application you need some application to modify request headers so you can add api key

.. code-block:: console
    
    $Authorization ApiKey 77d5a3ce97ccff226dcdaaf07d5721f5
