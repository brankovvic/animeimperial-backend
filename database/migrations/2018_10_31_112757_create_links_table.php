<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('server_id')->index();
            $table->unsignedInteger('episode_id')->index();
            $table->foreign('server_id')->references('id')->on('link_servers')->onDelete('cascade');
            $table->foreign('episode_id')->references('id')->on('episodes')->onDelete('cascade');
            $table->enum('status', ['online', 'offline', 'bad_link'])->default('offline');
            $table->dateTime('lastChecked')->nullable();
            $table->enum('extract_link', [1, 0])->default(true);
            $table->string('link');
            $table->string('direct_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
