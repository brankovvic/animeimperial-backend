<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_views', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('episode_id')->nullable()->undex();
            $table->foreign('episode_id')->references('id')->on('episodes')->onDelete('cascade');
            $table->unsignedInteger('series_id')->nullable()->undex();
            $table->foreign('series_id')->references('id')->on('series')->onDelete('cascade');
            $table->ipAddress('client_ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_views');
    }
}
