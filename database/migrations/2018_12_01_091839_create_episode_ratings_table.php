<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodeRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episode_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('episode_id')->undex();
            $table->foreign('episode_id')->references('id')->on('episodes')->onDelete('cascade');
            $table->smallInteger('like')->nullable();
            $table->smallInteger('dislike')->nullable();
            $table->ipAddress('client_ip');
            $table->dateTime('last_voted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('episode_ratings');
    }
}
