<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeriesViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW series_view AS
SELECT `series`.*, ss.`rating_count` as ratingCount, ss.`rating_number` as ratingNumber, ss.`score_number` as scoreNumber, ss.`score_count` as scoreCount, s.name as studioName, s.slug as studioSlug, suw.`view_number` as viewNumber
FROM `series`
LEFT JOIN series_statistics as ss
ON series.id = ss.`series_id`
LEFT JOIN series_user_views as suw
ON series.id = suw.`series_id`
INNER JOIN studios as s
ON series.`studio_id`=s.id;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('series_view');
    }
}
