<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('series', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->unsignedInteger('studio_id')->index();
            $table->foreign('studio_id')->references('id')->on('studios')->onDelete('cascade');
            $table->string('aired');
            $table->string('status');
            $table->string('premiere');
            $table->text('description');
            $table->tinyInteger('series_completed')->default(0);
            $table->tinyInteger('import_status')->default(0);
            $table->tinyInteger('dubbed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('series');
    }
}
