<?php

use App\Series;
use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    return [
        'series_id'=>function() {
            return factory(Series::class)->create()->id;
        },
        'rating_number'=>$faker->randomFloat(),
        'rating_count'=>$faker->randomNumber(),
        'score_number'=>$faker->randomFloat(),
        'score_count'=>$faker->randomNumber(),
    ];
});
