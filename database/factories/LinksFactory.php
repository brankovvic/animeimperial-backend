<?php

use App\Episode;
use App\Links;
use App\LinkServer;
use Faker\Generator as Faker;

$factory->define(Links::class, function (Faker $faker) {
    return [
        'server_id'=>function() {
        return factory(LinkServer::class)->create()->id;
        },
        'episode_id'=>function() {
            return factory(Episode::class)->create()->id;
        },
        'status'=>$faker->randomElement(['online', 'offline', 'bad_link']),
        'lastChecked'=>$faker->dateTime,
        'extract_link'=>$faker->randomElement(['1', '0']),
        'link'=>$faker->url,
        'direct_link'=>$faker->url
    ];
});


