<?php

use App\Series;
use App\Studio;
use Faker\Generator as Faker;

$factory->define(
    Series::class,
    function (Faker $faker) {
        $title = $faker->title;

        return [
            'title' => $title,
            'slug' => str_slug($title),
            'studio_id' => function () {
                return factory(Studio::class)->create()->id;
            },
            'aired' => $faker->dateTime,
            'status' => $faker->randomElement(['Completed', 'In Progress']),
            'premiere' => $faker->randomElement(['Fall 2006', 'Summer 2014', 'Spring 2009']),
            'quality' => $faker->randomElement(['HD', 'SD']),
            'description' => $faker->text,
            'series_completed' => $faker->randomElement(['0', '1']),
            'dubbed' => $faker->randomElement(['0', '1']),
        ];
    }
);

$factory->define(
    Series::class,
    function (Faker $faker) {
        return [
            'series_id' => function () {
                return factory(Series::class)->create()->id;
            },
            'view_number' => $faker->randomNumber(),
        ];
    }
);
