<?php

use App\Episode;
use App\Series;
use Faker\Generator as Faker;

$factory->define(Episode::class, function (Faker $faker) {
    $name = $faker->name;
    return [
        'series_id'=> function() {
        return factory(Series::class)->create()->id;
        },
        'episode_number'=>$faker->randomNumber(),
        'title'=>$name,
        'slug'=>str_slug($name),
        'description'=>$faker->text,
        'aired'=>$faker->dateTime
    ];
});
