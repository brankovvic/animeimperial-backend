<?php

use App\Genre;
use App\Series;
use App\SeriesGenre;
use Faker\Generator as Faker;

$factory->define(Genre::class, function (Faker $faker) {
    $name = $faker->name;
    return [
        'name'=>$name,
        'slug'=>str_slug($name)
    ];
});

$factory->define(SeriesGenre::class, function (Faker $faker) {
    $name = $faker->name;
    return [
        'genre_id'=>function() {
            return factory(Genre::class)->create()->id;
        },
        'series_id'=>function() {
            return factory(Series::class)->create()->id;
        },
    ];
});
