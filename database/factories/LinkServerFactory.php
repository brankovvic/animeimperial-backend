<?php

use App\LinkServer;
use Faker\Generator as Faker;

$factory->define(LinkServer::class, function (Faker $faker) {
    return [
        'name'=>$faker->name
    ];
});
