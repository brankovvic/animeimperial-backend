<?php

use App\Studio;
use Faker\Generator as Faker;

$factory->define(Studio::class, function (Faker $faker) {
    $name = $faker->name;
    return [
        'name'=>$name,
        'slug'=>str_slug($name)
    ];
});
